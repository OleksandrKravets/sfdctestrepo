public class CreateInvoiceClass {

    public CreateInvoiceClass() {

    }
    public static void createInvoice(Integer invoiceNumber) {
        List<Invoice__c> listInvoice = new List<Invoice__c>();
        List<String> currencyList = new List<String>();
        currencyList.add('USD');
        currencyList.add('EUR');
        currencyList.add('RUB');
        currencyList.add('GBP');
        Integer upperLimit = currencyList.size();

        for (Integer i = 1; i <= invoiceNumber; i++) {
            Integer rand = randomWithLimit(upperLimit);
            Invoice__c invoiceOne = new Invoice__c (Name = 'TestInvoice' + String.valueOf(i),   Amount__c = i * 10, Currency__c = currencyList[rand]);
            listInvoice.add(invoiceOne);
        }
        insert listInvoice;
    }

    public static void deleteInvoice() {
        List<Invoice__c> listInvoice = new List<Invoice__c>([SELECT Id, Name FROM Invoice__c]);
        delete listInvoice;
    }


    public static Integer randomWithLimit(Integer upperLimit) {
        Integer rand = Math.round(Math.random() * 4);
        return Math.mod(rand, upperLimit);
    }
}