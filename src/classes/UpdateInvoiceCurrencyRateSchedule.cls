public class UpdateInvoiceCurrencyRateSchedule implements Schedulable {

	public void execute(SchedulableContext sc) {
		database.executebatch(new UpdateInvoiceCurrencyRateBatch());
	}
}
//String strSchedule = '0 0 * * * ?';
//System.schedule('Update Invoice CurrencyRate every hour', strSchedule, new UpdateInvoiceCurrencyRateSchedule());