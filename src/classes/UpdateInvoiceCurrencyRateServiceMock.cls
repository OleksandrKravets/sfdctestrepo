@isTest
global class UpdateInvoiceCurrencyRateServiceMock implements HttpCalloutMock {

    // Implement this interface method
    global HTTPResponse respond(HTTPRequest request) {
        // Optionally, only send a mock response for a specific endpoint
        // and method.
        System.assertEquals('http://api.fixer.io/latest?base=USD&symbols=EUR,RUB,GBP', request.getEndpoint());
        System.assertEquals('GET', request.getMethod());
        
        // Create a fake response
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('{"base":"USD","date":"2017-06-26","rates":{"GBP":0.78511,"RUB":59.032,"EUR":0.89389}}');
        response.setStatusCode(200);
        return response;
    }

}