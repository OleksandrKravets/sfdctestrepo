public class BatchUpdateInvoiceCurrencyRate implements Database.Batchable<sObject>, Database.AllowsCallouts {

	public static final String CURRENCY_RATES = 'http://api.fixer.io/latest?base=USD&symbols=EUR,RUB,GBP';
	public final String QUERY = 'SELECT Id, Currency__c, Currency_Rate__c FROM Invoice__c ';
	List <Invoice__c> invoiceToUpdate = new List<Invoice__c>();

	public BatchUpdateInvoiceCurrencyRate() {

	}

	public Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(QUERY);
	}

	public void execute(Database.BatchableContext BC, List<Invoice__c> scope) {

		String currencyRates = getInfoFromExternalService().getBody().substring(43, 83);
		String text = currencyRates.remove('"');
		String [] token = text.split(',');
		String [] gbp = token[0].split(':');
		String [] rub = token[1].split(':');
		String [] eur = token[2].split(':');


		for (Invoice__c cr : scope) {
			if (cr.Currency__c == 'GBP') {
				cr.Currency_Rate__c = Decimal.valueOf(gbp[1]);
			} else if (cr.Currency__c == 'RUB') {
				cr.Currency_Rate__c = Decimal.valueOf(rub[1]);
			} else if (cr.Currency__c == 'EUR') {
				cr.Currency_Rate__c = Decimal.valueOf(eur[1]);
			} else {
				cr.Currency_Rate__c = 1.0;
			}
			invoiceToUpdate.add(cr);
		}
		update invoiceToUpdate;
	}

	public void finish(Database.BatchableContext BC) {

	}

	public static HttpResponse getInfoFromExternalService() {
		Http httpProtocol = new Http();

		HttpRequest request = new HttpRequest();
		request.setEndpoint(CURRENCY_RATES);
		request.setMethod('GET');
		request.setHeader('Content-Type', 'application/json');

		HttpResponse response = httpProtocol.send(request);
		return response;
	}
}