public class ExpensesController {

	@AuraEnabled
	public static List<Expense__c> getExpenses() {
		return [Select Id, Name, Amount__c, Client__c, Date__c, Reimbursed__c FROM Expense__c];
	}

	@AuraEnabled
	public static Expense__c saveExpense(Expense__c exp) {
		upsert exp;
		return exp;
	}


}