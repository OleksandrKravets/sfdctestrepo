public class ContactClassRollUpHandler {
	private Map<Id, Integer> pupilsNumberIncrementByClassId;
	private Map<Id, Integer> pupilsNumberDecrementByClassId;

	public ContactClassRollUpHandler() {
		pupilsNumberIncrementByClassId = new Map<Id, Integer>();
		pupilsNumberDecrementByClassId = new Map<Id, Integer>();
	}

	public void incrementNumberOfPupils(Contact contactInScope) {
		if (!pupilsNumberIncrementByClassId.containsKey(contactInScope.Class__c)) {
			pupilsNumberIncrementByClassId.put(contactInScope.Class__c, 0);
		}
		Integer previousValue = pupilsNumberIncrementByClassId.get(contactInScope.Class__c);
		pupilsNumberIncrementByClassId.put(contactInScope.Class__c, ++previousValue);
	}


	public List<Class__c> incrementClassNumberOfPupils() {
		List<Class__c> result = [SELECT Id, Number_of_populs__c FROM Class__c WHERE Id IN :pupilsNumberIncrementByClassId.keySet()];
		for (Class__c classToUpdate : result) {
			if (classToUpdate.Number_of_populs__c == null) {
				classToUpdate.Number_of_populs__c = 0;
			}
			classToUpdate.Number_of_populs__c += pupilsNumberIncrementByClassId.get(classToUpdate.Id);
		}
		return result;
	}

	public void decrementNumberOfPupils(Contact oldContactInScope) {
		if (!pupilsNumberDecrementByClassId.containsKey(oldContactInScope.Class__c)) {
			pupilsNumberDecrementByClassId.put(oldContactInScope.Class__c, 0);
		}
		Integer previousValue = pupilsNumberDecrementByClassId.get(oldContactInScope.Class__c);
		pupilsNumberDecrementByClassId.put(oldContactInScope.Class__c, ++previousValue);
	}


	public List<Class__c> decrementClassNumberOfPupils() {
		List<Class__c> result = [SELECT Id, Number_of_populs__c FROM Class__c WHERE Id IN :pupilsNumberDecrementByClassId.keySet()];
		for (Class__c classToUpdate : result) {
			if (classToUpdate.Number_of_populs__c == null) {
				classToUpdate.Number_of_populs__c = 0;
			}
			classToUpdate.Number_of_populs__c -= pupilsNumberDecrementByClassId.get(classToUpdate.Id) ;
		}
		return result;
	}


	public void updateNumberOfPopils(Contact oldContactInScope, Contact contactInScope) {
		if (oldContactInScope.Class__c != contactInScope.Class__c) {
			decrementNumberOfPupils(oldContactInScope);
			incrementNumberOfPupils(contactInScope);
		}

	}


	public List<Class__c> prepairForDatabaseUpdate (String operation) {
		List<Class__c> classesToUpdate = new List<Class__c>();
		List<Class__c> classesToDecrement = new List<Class__c>();
		if (operation.equals('insert')) {
			classesToUpdate = incrementClassNumberOfPupils();
		}

		else if (operation.equals('update')) {
			classesToUpdate = incrementClassNumberOfPupils();
			classesToDecrement = decrementClassNumberOfPupils();
			if (!classesToDecrement.isEmpty() & !classesToUpdate.isEmpty()) {
				classesToUpdate.addAll(classesToDecrement);
			}
		}


		else if (operation.equals('delete')) {
			classesToUpdate = decrementClassNumberOfPupils();
		}
		return classesToUpdate;
	}
}