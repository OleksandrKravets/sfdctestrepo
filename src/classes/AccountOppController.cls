public class AccountOppController {
 @AuraEnabled
    public static list<Account> getAccounts(){
        return [SELECT Id, Name FROM Account LIMIT 10];
    }

    @AuraEnabled
    public static list<Opportunity> getOpportunities(Id AccountId){
        return [SELECT Id, Name FROM Opportunity WHERE AccountId = :AccountId];
    }

}