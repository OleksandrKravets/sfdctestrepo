public with sharing class QuickManufacturerController {
     
    @AuraEnabled
    public static Opportunity getOpportunity(Id opportunityId) {
       return [SELECT Name FROM Opportunity WHERE Id = :opportunityId];
    }
        
           
       @AuraEnabled
       public static List<Tender_Vehicle__c> getTenderVehicle() {
        return [SELECT Id, Name, Related_Tender__r.Name, Related_Vehicle__r.Name FROM Tender_Vehicle__c];
    }
    
    
      @AuraEnabled
      public static Tender_Vehicle__c saveVehicleWithTender(Tender_Vehicle__c tenderVehicle, Id vehicleId, Id tenderId) {
        tenderVehicle.Related_Tender__c  = tenderId; 
        tenderVehicle.Related_Vehicle__c  = vehicleId; 
        upsert tenderVehicle;
        return tenderVehicle;
    } 
      
    
       @AuraEnabled
       public static List<Vehicle__c> getVehicles(Id manufacturerId) {
       	    return [SELECT Id, Name, Manufacturer__c, Manufacturer__r.Name FROM Vehicle__c ]; /*
          return QuickManufacturerController.getVehiclesByManufacturerId().get(manufacturerId);
        List< Vehicle__c > listVehicleToShow;
        Map<Id, SObject> vehiclesByManufacturerId = new Map<Id, SObject>([SELECT Id, (SELECT Id, Name FROM Vehicles__r) 
                                                                           FROM Manufacturer__c WHERE Manufacturer__c =:manufacturerId]);
         	 for(Vehicle__c v : listVehicle) {
                 if(vehiclesByManufacturerId.containsKey(v.Manufacturer__c)){
                     listVehicleToShow.add(v);
                 }
             }
           return listVehicleToShow;
           
           Map<Id, List<Vehicle__c>> vehiclesByManufacturerId = new Map<Id, List<Vehicle__c>>();
           for (Vehicle__c vehicle : [SELECT Id, Name, Manufacturer__c FROM Vehicle__c LIMIT 100]) {
               if (!vehiclesByManufacturerId.containsKey(vehicle.Manufacturer__c)) {
                   vehiclesByManufacturerId.put(vehicle.Manufacturer__c, new List<Vehicle__c>);
               }
               vehiclesByManufacturerId.get(vehicle.Manufacturer__c).add(vehicle);
           }
    	 */
	}
                 
}