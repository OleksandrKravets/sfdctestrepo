@isTest
private class UpdateInvoiceCurrencyRateBatchTest  {


	@isTest static void testUpdateInvoiceCurrencyRateFromService() {

		// Set mock callout class
		Test.setMock(HttpCalloutMock.class, new UpdateInvoiceCurrencyRateServiceMock());
		
		Test.startTest();
		createInvoice();
		Database.executeBatch(new UpdateInvoiceCurrencyRateBatch(), 100);
		Test.stopTest();

		Decimal testCount = [SELECT Currency_Rate__c FROM Invoice__c Where Currency__c = 'EUR'][0].Currency_Rate__c;
		System.assertEquals(0.89389, testCount, 'Success' );

	}

	static void createInvoice() {
		List<Invoice__c> listInvoice = new List<Invoice__c>();
		List<String> currencyList = new List<String>();
		currencyList.add('USD');
		currencyList.add('EUR');
		currencyList.add('RUB');
		currencyList.add('GBP');
		Integer upperLimit = currencyList.size();

		for (Integer i = 1; i <= 20; i++) {
			Integer rand = randomWithLimit();
			Invoice__c invoiceOne = new Invoice__c (Name = 'TestInvoice' + String.valueOf(i),   Amount__c = i * 10, Currency__c = currencyList[rand]);
			listInvoice.add(invoiceOne);
		}
		insert listInvoice;
	}


	static Integer randomWithLimit() {
		Integer rand = Math.round(Math.random() * 4);
		return Math.mod(rand, 4);
	}
}