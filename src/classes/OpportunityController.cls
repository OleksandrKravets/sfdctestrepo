public class OpportunityController {
    @AuraEnabled
    public static List<Opportunity> getOpportunities() {
        return [SELECT Id, Name, AccountId, Account.Name, Amount, CloseDate, Type, StageName FROM Opportunity LIMIT 20];
    }
        
    @AuraEnabled
    public static Opportunity saveOpportunity(Opportunity opportunity) {
        upsert opportunity;
        return opportunity;
    }
}