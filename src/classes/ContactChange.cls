/**
 * Class AccountGateway
 *
 * Provides finder methods for accessing data in the Account object.
 */
public class ContactChange {
	/**
	 * Returns a subset of id's where there are any records in use.
	 *
	 * Arguments:	Set<Id> accIds - Set of Account Id's to examine
	 *
	 * Returns:		Set<Id> - Set of Account Id's that are 'in use'
	 */
	public  Set<Id> findContactIdsInUse(List<Contact> contactList) {
		Set<Id> classId = new Set <Id>();

		for (Contact contact : contactList) {
			if (contact.Class__c != null) {
				classId.add(contact.Class__c);
			}
		}
		return classId;

	}


	public  Map<Id, Class__c> incrementNumberOfPopuls(List<Contact> contactList, Map<Id, Class__c> classMap) {

		for (Contact contact : contactList) {
			if (classMap.containsKey(contact.Class__c)) {
				if (classMap.get(contact.Class__c).Number_of_populs__c == null) {
					classMap.get(contact.Class__c).Number_of_populs__c = 1;
				} else {
					classMap.get(contact.Class__c).Number_of_populs__c += 1;
				}
			}
		}
		return classMap;
		
	}

}