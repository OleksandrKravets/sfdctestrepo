public class UpdateInvoiceCurrencyRateBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {

	private static final String CURRENCY_RATES = 'http://api.fixer.io/latest?base=USD&symbols=EUR,RUB,GBP';
	private static final String INVOICE_QUERY = 'SELECT Id, Currency__c, Currency_Rate__c FROM Invoice__c';
	private static final String REQUEST_HEADER = 'application/json';


	public UpdateInvoiceCurrencyRateBatch() {

	}

	public Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator(INVOICE_QUERY);
	}

	public void execute(Database.BatchableContext BC, List<Invoice__c> scope) {
		String currencyRateString = getResponseFromCurrencyRateService();
		Map <String, Decimal> currencyRateMap = getCurrencyRateMap(currencyRateString);

		for (Invoice__c invoice : scope) {
			if (currencyRateMap.containsKey(invoice.Currency__c)) {
				invoice.Currency_Rate__c = currencyRateMap.get(invoice.Currency__c);
			} else {
				invoice.Currency_Rate__c = 1.0;
			}

		}
		update scope;
	}

	public void finish(Database.BatchableContext BC) {

	}

	private  String getResponseFromCurrencyRateService() {
		Http httpProtocol = new Http();
		HttpRequest request = new HttpRequest();
		request.setEndpoint(CURRENCY_RATES);
		request.setMethod('GET');
		request.setHeader('Content-Type', REQUEST_HEADER);
		HttpResponse response = httpProtocol.send(request);
		return response.getBody();
	}


	private Map <String, Decimal> getCurrencyRateMap (String currencyRateString) {
		String currencyRateToArray = currencyRateString.substringBetween('{', '}').substringAfter('{').remove('"');
		String [] currencyRateToMap = currencyRateToArray.split(',');
		Map <String, Decimal> currencyRateMap = new Map <String, Decimal>();
		currencyRateMap.put(currencyRateToMap[0].split(':')[0], Decimal.valueOf(currencyRateToMap[0].split(':')[1]));
		currencyRateMap.put(currencyRateToMap[1].split(':')[0], Decimal.valueOf(currencyRateToMap[1].split(':')[1]));
		currencyRateMap.put(currencyRateToMap[2].split(':')[0], Decimal.valueOf(currencyRateToMap[2].split(':')[1]));
		return currencyRateMap;
	}
}