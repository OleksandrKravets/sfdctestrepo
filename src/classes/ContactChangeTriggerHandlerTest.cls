@isTest
private class ContactChangeTriggerHandlerTest {

  @isTest
  static void verifyBehaviorOnInsert_positive() {
    List<Class__c> classes = initClass(2);
    insert classes;

    List<Contact> listContact = initContact(classes, 10);

    Test.startTest();
    insert listContact;
    Test.stopTest();

    Decimal testCount = [SELECT Number_of_populs__c FROM Class__c Where Id IN :classes][0].Number_of_populs__c;
    System.assertEquals(10, testCount, 'Success' );
  }

  @isTest
  static void verifyBehaviorOnDelete_positive() {
    List<Class__c> classes = initClass(2);
    insert classes;

    List<Contact> listContact = initContact(classes, 10);
    insert listContact;
    Test.startTest();
    delete listContact;

    Test.stopTest();
    Decimal testCount = [SELECT Number_of_populs__c FROM Class__c Where Id IN :classes][0].Number_of_populs__c;

    System.assertEquals(0, testCount, 'Success' );
  }

  @isTest
  static void verifyBehaviorOnUpdate_positive() {
    List<Class__c> classes = initClass(2);
    insert classes;

    List<Contact> listContact = initContact(classes, 10);
    insert listContact;

    Test.startTest();
    listContact[0].Class__c = classes[1].Id;
    listContact[1].Class__c = classes[0].Id;
    update listContact;

    Test.stopTest();

    Decimal testCount = [SELECT Number_of_populs__c FROM Class__c Where Id IN :classes][0].Number_of_populs__c;
    System.assertEquals(9, testCount, 'Success' );
  }


  private static List<Class__c> initClass(Integer ClassNumber) {
    School__c schoolOne = new School__c (Name = 'TestSchool№1');
    insert schoolOne;

    List<Class__c> classes = new List<Class__c>();
    for (Integer i = 0; i < ClassNumber; i++) {
      Class__c classOne = new Class__c (Name = 'TestClass№' + String.valueOf(i), School__c = schoolOne.Id);
      classes.add(classOne);
    }
    return classes;
  }


  private static List<Contact> initContact(List<Class__c> classes, Integer ContactNumber) {
    List<Contact> listContact = new List<Contact>();
    for (Integer j = 0; j < classes.size(); j++) {
      Class__c classOne = classes[j];
      for (Integer k = 0; k < ContactNumber; k++) {
        Contact contact = new Contact (LastName = 'TestContact' + String.valueOf(k), Class__c = classOne.id);
        listContact.add(contact);
      }
    }
    return listContact;
  }
}