/**
 * Class AccountHandler
 *
 * Trigger Handler for the Account SObject. This class implements the ITrigger
 * interface to help ensure the trigger code is bulkified and all in one place.
 */
public class ContactHandler implements ITrigger {
    ContactClassRollUpHandler classRollUpHandler;

    public ContactHandler() {
        this.classRollUpHandler = new ContactClassRollUpHandler();
    }

    public void bulkBefore() {
    }

    public void bulkAfter() {
    }

    public void beforeInsert(SObject so) {
    }

    public void beforeUpdate(SObject oldSo, SObject so) {
    }

    public void beforeDelete(SObject so) {

    }

    public void afterInsert(SObject so) {
        classRollUpHandler.incrementNumberOfPupils((Contact) so);
    }

    public void afterUpdate(SObject oldSo, SObject so) {
        classRollUpHandler.updateNumberOfPopils((Contact) oldSo, (Contact) so);

    }

    public void afterDelete(SObject so) {
        classRollUpHandler.decrementNumberOfPupils((Contact) so);
    }

    public void andFinally() {
        if (Trigger.isInsert) {
            List<Class__c> classesToUpdate = classRollUpHandler.prepairForDatabaseUpdate('insert');
            if (!classesToUpdate.isEmpty()) {
                Database.update(classesToUpdate);
            }
        }

        else if (Trigger.isUpdate) {
            List<Class__c> classesToUpdate = classRollUpHandler.prepairForDatabaseUpdate('update');
            if (!classesToUpdate.isEmpty()) {
                Database.update(classesToUpdate);
            }
        }

        else if (Trigger.isDelete) {
            List<Class__c> classesToUpdate = classRollUpHandler.prepairForDatabaseUpdate('delete');
            if (!classesToUpdate.isEmpty()) {
                Database.update(classesToUpdate);
            }
        }
    }
}