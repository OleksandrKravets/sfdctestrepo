@isTest
private class CurrencyRateUpdateWithBatchTest  {



	@isTest static void testCallout() {

		// Set mock callout class
		Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());

		// Call method to test.
		// This causes a fake response to be sent
		// from the class that implements HttpCalloutMock.
		HttpResponse response = BatchUpdateInvoiceCurrencyRate.getInfoFromExternalService();

		// Verify response received contains fake values
		String contentType = response.getHeader('Content-Type');
		System.assert(contentType == 'application/json');
		String actualValue = response.getBody();
		String expectedValue = '{"base":"USD","date":"2017-06-26","rates":{"GBP":0.78511,"RUB":59.032,"EUR":0.89389}}';
		System.assertEquals(actualValue, expectedValue);
		System.assertEquals(200, response.getStatusCode());



		Test.startTest();
		createInvoice();
		Database.executeBatch(new BatchUpdateInvoiceCurrencyRate(), 100);
		Test.stopTest();

		Decimal testCount = [SELECT Currency_Rate__c FROM Invoice__c Where Currency__c = 'EUR'][0].Currency_Rate__c;
		System.assertEquals(0.89389, testCount, 'Success' );

	}

	@isTest static void createInvoice() {
		List<Invoice__c> listInvoice = new List<Invoice__c>();
		List<String> currencyList = new List<String>();
		currencyList.add('USD');
		currencyList.add('EUR');
		currencyList.add('RUB');
		currencyList.add('GBP');
		Integer upperLimit = currencyList.size();

		for (Integer i = 1; i <= 20; i++) {
			Integer rand = randomWithLimit();
			Invoice__c invoiceOne = new Invoice__c (Name = 'TestInvoice' + String.valueOf(i),   Amount__c = i * 10, Currency__c = currencyList[rand]);
			listInvoice.add(invoiceOne);
		}
		insert listInvoice;
	}


	@isTest static Integer randomWithLimit() {
		Integer rand = Math.round(Math.random() * 4);
		return Math.mod(rand, 4);
	}
}