@isTest
public class TestDataFactory {
	public static List<Contact> createContactsWithClasses(Integer ContactNumber, Integer ClassNumber) {
		List<Class__c> classes = new List<Class__c>();
		for (Integer i = 0; i < ClassNumber; i++) {
			Class__c classOne = new Class__c (Name = 'TestClass№' + i);
			classes.add(classOne);
		}
		insert classes;


		List<Contact> listContact = new List<Contact>();
		for (Integer j = 0; j < ClassNumber; j++) {
			Class__c classOne = classes[j];
			for (Integer k = 0; k < ContactNumber; j++) {
				listContact.add(new Contact (LastName = 'TestContact' + k, Class__c = classOne.id));
			}
		}
		insert listContact;
		return listContact;
	}
}