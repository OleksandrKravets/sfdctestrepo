global class ScheduleUpdateInvoiceCurrencyRate implements Schedulable {

	global void execute(SchedulableContext sc) {
		database.executebatch(new BatchUpdateInvoiceCurrencyRate());
	}
}
//String strSchedule = '0 0 * * * ?';
//System.schedule('Update Invoice CurrencyRate', strSchedule, new ScheduleUpdateInvoiceCurrencyRate());