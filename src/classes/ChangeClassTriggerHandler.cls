public class ChangeClassTriggerHandler {

  public void incrementNumberOfPopuls(List<Contact> contactList) {

    Set<Id> classId = new Set <Id>();
    for (Contact contact : contactList) {
      if (contact.Class__c != null) {
        classId.add(contact.Class__c);
      }
    }
    Map<Id, Class__c> classMap = new Map<Id, Class__c>([SELECT Id, Number_of_populs__c FROM Class__c where Id IN :classId]);
    for (Contact contact : contactList) {
      if (classMap.containsKey(contact.Class__c)) {
        if (classMap.get(contact.Class__c).Number_of_populs__c == null) {
          classMap.get(contact.Class__c).Number_of_populs__c = 1;
        } else {
          classMap.get(contact.Class__c).Number_of_populs__c += 1;
        }
      }
    }
    try {
      Database.upsert(classMap.values());
    } catch (DMLException dmlEx) {
      // Going through all errors
      for ( Integer i = 0; i < dmlEx.getNumDml(); i++ ) {
        String errorMessage = 'Error : ' + dmlEx.getDmlMessage( i );
      }
    }
  }
}