trigger changeNumberOfPopulsClass on Contact (after insert, after update, after delete) {
	
    if(Trigger.isInsert || Trigger.isUpdate) {
      new ChangeClassTriggerHandler().incrementNumberOfPopuls(trigger.new);
   }
   
  	if(Trigger.isDelete) {
      new ChangeClassTriggerHandler().incrementNumberOfPopuls(trigger.old);

    }
  
}

//- Consistent naming: TenderPageOne -> NewTenderScreen, OpportunityController -> NewTenderScreenController
//- SOLID priciples: Single-Responsibility Priciple
//- Inconsistent spacing
//- Limit SOQL scope by using WHERE, Limit
//- With sharing/without sharing over usage
//- Map<Id, List<Related__c>> relatedRecordsByParentId concept
//- Lightning interfaces cleanup
//- InputSelectOption over usage - better dynamical field picklist values
//-  Schema.DescribeFieldResult  getPicklistValues()