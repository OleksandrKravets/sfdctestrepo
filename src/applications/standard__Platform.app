<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <tab>standard-Chatter</tab>
    <tab>standard-Account</tab>
    <tab>standard-Contact</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>Vehicle__c</tab>
    <tab>Manufacturer__c</tab>
    <tab>Expense__c</tab>
    <tab>Tender_Vehicle__c</tab>
    <tab>Class__c</tab>
    <tab>School__c</tab>
    <tab>Invoice__c</tab>
</CustomApplication>
