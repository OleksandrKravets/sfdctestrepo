({
	getTender : function(component, event, helper) {
        var action = component.get("c.getOpportunity");
        action.setCallback(this, function(data) {
        component.set("v.opp", data.getReturnValue());
});
$A.enqueueAction(action);
		
	}
})

({
    clickCreateTender: function(component, event, helper) {

        // Simplistic error checking
        var validTender = true;

        // Name must not be blank
        var nameField = component.find("tendername");
        var tendername = nameField.get("v.value");
        if ($A.util.isEmpty(tendername)){
            validTender = false;
            nameField.set("v.errors", [{message:"Tender name can't be blank."}]);
        }
        else {
            nameField.set("v.errors", null);
        }

        // ... hint: more error checking here ...

        // If we pass error checking, do some real work
        if(validTender){
            // Create the new tender
            var newTender = component.get("v.newtenderform");
            console.log("Create tender: " + JSON.stringify(newTender));
            helper.createTender(component, newTender);
        }
    }
})


({
    createTender: function(component, tender) {
        var theTenders = component.get("v.tenders");
 
        // Copy the expense to a new object
        // THIS IS A DISGUSTING, TEMPORARY HACK
        var newTender = JSON.parse(JSON.stringify(tender));
 
        theExpenses.push(newTender);
        component.set("v.tenders", theTenders);
    }
})