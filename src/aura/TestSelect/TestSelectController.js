({
   
    doInit : function(component, event, helper) {
    var action = component.get("c.getStageStatus");
    var inputsel = component.find("InputSelectDynamic");
     var opts = [
            { "class": "optionClass", label: "Option1", value: "opt1", selected: "true" },
            { "class": "optionClass", label: "Option2", value: "opt2" },
            { "class": "optionClass", label: "Option3", value: "opt3" }

        ];
    action.setCallback(this, function(a) {
        for(var i=0;i< a.getReturnValue().length;i++){
            opts.push({"class": "optionClass", label: a.getReturnValue()[i], value: a.getReturnValue()[i]});
        }
        inputsel.set("v.options", opts);

    });
   // $A.enqueueAction(action); 
},
    
    
    
    
    
  /*  doInit : function(cmp) {
    	// Initialize input select options
        var opts = [
            { "class": "optionClass", label: "Option1", value: "opt1", selected: "true" },
            { "class": "optionClass", label: "Option2", value: "opt2" },
            { "class": "optionClass", label: "Option3", value: "opt3" }

        ];
        cmp.find("InputSelectDynamic").set("v.options", opts);
        
    },*/
    
   
    loadOptions: function (component, event, helper) {
       var opts = [
            { value: "Red", label: "Red" },
            { value: "Green", label: "Green" },
            {  value: "Blue", label: "Blue" }
         ];
        component.set("v.options", opts);
        component.set('v.selectedValue', 'Red');
        //return the selected value
        component.find("mySelect").get("v.value");

        
    },

    

	onSingleSelectChange: function(component) {
         var selectCmp = component.find("InputSelectSingle");
         var resultCmp = component.find("singleResult");
         resultCmp.set("v.value", selectCmp.get("v.value"));
      
        
	 },

	 onMultiSelectChange: function(cmp) {
         var selectCmp = cmp.find("InputSelectMultiple");
         var resultCmp = cmp.find("multiResult");
         resultCmp.set("v.value", selectCmp.get("v.value"));
	 },
	 
	 onChange: function(cmp) {
		 var dynamicCmp = cmp.find("InputSelectDynamic");
		 var resultCmp = cmp.find("dynamicResult");
		 resultCmp.set("v.value", dynamicCmp.get("v.value"));
	 }
	 
})