({
    handleManufacturerChange : function(component, event, helper) {
       
        var selectVehicle = component.find("vehicle");
        component.set("v.vehicleOptions",
        selectVehicle.optionsByControllingValue[event.target.value]);
    },
    
	handleInit : function(component, event, helper) {
        var self = this;  // safe reference
		var getVehicles = component.get("c.getVehicles");
        getVehicles.setCallback(self, function(a) {
            var vehicles = a.getReturnValue();  
            
            // Construct the list of Account picklist options
            var manufacturerOptions = [];
            var vehicleOptionsByManufacturertId = new Object();
            
            vehicles.forEach(function(element, index, array) {
                var manufacturerId = element.Manufacturer__c;
                if (vehicleOptionsByManufacturertId[manufacturerId] === undefined) {
                    var manufacturerOption = new Object();
                    manufacturerOption.value = element.Manufacturer__c;
                    manufacturerOption.label = element.Manufacturer__r.Name;
                    manufacturerOptions.push(manufacturerOption);
                    vehicleOptionsByManufacturertId[manufacturerId] = [];
                }
                
               
                var vehicleOption = new Object();
                vehicleOption.value = element.Id;
                vehicleOption.label = element.Name;
                vehicleOptionsByManufacturertId[manufacturerId].push(vehicleOption);
            });
            
            // Set the Manufacturer options
            component.set("v.manufacturerOptions", manufacturerOptions);
            var selectVehicle = component.find("vehicle");
            selectVehicle.optionsByControllingValue = vehicleOptionsByManufacturertId;
        });
        $A.enqueueAction(getVehicles);      
    }
})