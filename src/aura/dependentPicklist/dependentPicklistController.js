({
    handleAccountChange : function(component, event, helper) {
        
        
        var selectContact = component.find("contact");
        component.set("v.contactOptions",
        selectContact.optionsByControllingValue[event.target.value]);
    },
    
	handleInit : function(component, event, helper) {
        var self = this;  // safe reference
		var getContacts = component.get("c.getContacts");
        getContacts.setCallback(self, function(a) {
            var contacts = a.getReturnValue();  // Array<Object>
            
            // Construct the list of Account picklist options
            var accountOptions = [];
            var contactOptionsByAccountId = new Object();
            
            contacts.forEach(function(element, index, array) {
                var accountId = element.AccountId;
                if (contactOptionsByAccountId[accountId] === undefined) {
                    var accountOption = new Object();
                    accountOption.value = element.AccountId;
                    accountOption.label = element.Account.Name;
                    accountOptions.push(accountOption);
                    contactOptionsByAccountId[accountId] = [];
                }
                
               
                var contactOption = new Object();
                contactOption.value = element.Id;
                contactOption.label = element.Name;
                contactOptionsByAccountId[accountId].push(contactOption);
            });
            
            // Set the Account options
            component.set("v.accountOptions", accountOptions);
            var selectContact = component.find("contact");
            selectContact.optionsByControllingValue = contactOptionsByAccountId;
        });
        $A.enqueueAction(getContacts);      
    }
})