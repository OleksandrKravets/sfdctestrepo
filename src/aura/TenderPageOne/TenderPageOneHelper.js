({
    createOpportunity: function(component, opportunity) {
    var action = component.get("c.saveOpportunity");
    action.setParams({
        "opportunity": opportunity
     });
    action.setCallback(this, function(response){
        var state = response.getState();
        if (component.isValid() && state === "SUCCESS") {
            var opportunities = component.get("v.opportunities");
            opportunities.push(response.getReturnValue());
            component.set("v.opportunities", opportunities);
        }
    });
        $A.enqueueAction(action);
        
        var evt = $A.get("e.c:NavigateToPage2");
        evt.setParams({ "result": opportunity});
        evt.fire();

  
   // $A.enqueueAction(evt);
},
    
    
    validateOpportunityForm: function(component) {

    // Simplistic error checking
    var validOpportunity = true;

    // Name must not be blank
    var nameField = component.find("oppname");
    var oppname = nameField.get("v.value");
    if ($A.util.isEmpty(oppname)){
        validOpportunity = false;
        nameField.set("v.errors", [{message:"Opportunity name can't be blank."}]);
    }
    else {
        nameField.set("v.errors", null);
    }

    // Amount must be set, must be a positive number
    var amtField = component.find("amount");
    var amt = amtField.get("v.value");
    if ($A.util.isEmpty(amt) || isNaN(amt) || (amt <= 0.0)){
        validOpportunity = false;
        amtField.set("v.errors", [{message:"Enter an Opportunity amount."}]);
    }
    else {
        // If the amount looks good, unset any errors...
        amtField.set("v.errors", null);
    }
    
    return(validOpportunity);
}
    
})