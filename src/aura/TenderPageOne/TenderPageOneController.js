({
    clickCreateOpportunity: function(component, event, helper) {
    if(helper.validateOpportunityForm(component)){
        // Create the new Opportunity
        var newOpportunity = component.get("v.newOpportunity");
        helper.createOpportunity(component, newOpportunity);
      //  var evt = $A.get("e.c:NavigateToPage2");
      //  evt.fire();
		
    }
 },

// Load tenders from Salesforce
 doInit: function(component, event, helper) {
    // Create the action
 var action = component.get("c.getOpportunities");
    // Add callback behavior for when response is received
    action.setCallback(this, function(response) {
        var state = response.getState();
        if (component.isValid() && state === "SUCCESS") {
            component.set("v.opportunities", response.getReturnValue());
        }
        else {
            console.log("Failed with state: " + state);
        }
    });
    // Send action off to be executed
  //  $A.enqueueAction(action);
},
    handleInit : function(component, event, helper) {
    
		var accountOptions = component.get("c.getAccounts");
            // Construct the list of Account picklist options
          //  var accountOptions = [];
		    contacts.forEach(function(element, index, array) {
                var accountId = element.AccountId;
                    var accountOption = new Object();
                    accountOption.value = element.Account.Id;
                    accountOption.label = element.Account.Name;
                    accountOptions.push(accountOption);   
            });
           component.set("v.accountOptions", accountOptions);
       
     },
    
    typeSelectChange: function(cmp) {
         var selectCmp = cmp.find("select-01");
         var resultCmp = cmp.find("typeResult");
         resultCmp.set("v.value", selectCmp.get("v.value"));
	 },
    
    stageSelectChange: function(cmp) {
         var selectCmp = cmp.find("select-02");
         var resultCmp = cmp.find("stageResult");
         resultCmp.set("v.value", selectCmp.get("v.value"));
	 },
})