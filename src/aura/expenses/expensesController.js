({
    clickCreateExpense: function(component, event, helper) {
    if(helper.validateExpenseForm(component)){
        // Create the new expense
        var newExpense = component.get("v.newExpense");
        helper.createExpense(component, newExpense);
     
    }
},

// Load expenses from Salesforce
  doInit: function(component, event, helper) {
      
  /* var opts = [
            { value: "Open", label: "Open" },
            { value: "Closed", label: "Closed" },
            {  value: "Won", label: "Won" }
         ];
        component.set("v.options", opts);
        component.set('v.selectedValue', 'Open');
        
        */
        //return the selected value
        //component.find("mySelect").get("v.value");
      
      // Create the action
    var action = component.get("c.getExpenses");
    // Add callback behavior for when response is received
    action.setCallback(this, function(response) {
        var state = response.getState();
        if (component.isValid() && state === "SUCCESS") {
            component.set("v.expenses", response.getReturnValue());
        }
        else {
            console.log("Failed with state: " + state);
        }
    });
    // Send action off to be executed
    $A.enqueueAction(action);
      
      
},

    
          
    
})