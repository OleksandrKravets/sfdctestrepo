({
    handleAccountChange : function(component, event, helper) {
        
        // Get a reference to the dependent picklist
        var selectOpportunity = component.find("opportunity");
        
        // Call the helper function to refresh the
        // dependent picklist, based on the new controlling value
        component.set("v.opportunityOptions",
            selectOpportunity.optionsByControllingValue[event.target.value]);
    },
	handleInit : function(component, event, helper) {
        var self = this;  // safe reference
		
      
        var getOpportunities = component.get("c.getOpportunities");
        getOpportunities.setCallback(self, function(a) {
            var opportunities = a.getReturnValue();  // Array<Object>
            
          
            var accountOptions = [];
           
            var opportunityOptionsByAccountId = new Object();
            opportunities.forEach(function(element, index, array) {
                var accountId = element.AccountId;
                
                // If the contact's Account is new to us
                if (opportunityOptionsByAccountId[accountId] === undefined) {
                    
                 
                    var accountOption = new Object();
                    accountOption.value = element.AccountId;
                    console.log(element);
                    accountOption.label = element.Account.Name;
                    accountOptions.push(accountOption);
                    
                   
                    opportunityOptionsByAccountId[accountId] = [];
                }
                
                var opportunityOption = new Object();
                opportunityOption.value = element.Id;
                opportunityOption.label = element.Name;
                opportunityOptionsByAccountId[accountId].push(opportunityOption);
            });
            
            // Set the Account options
            component.set("v.accountOptions", accountOptions);
            
            // Attach the map of Contact options, keyed on
            // controlling Account ID values
            var selectOpportunity = component.find("opportunity");
            selectOpportunity.optionsByControllingValue = opportunityOptionsByAccountId;
        });
        $A.enqueueAction(getOpportunities);      
    }
})